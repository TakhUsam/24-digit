ymaps.ready(init);

const SKFO = `
[
    {
      "name": "Республика Дагестан",
      "count": 16399,
      "iso": "RU-DA"
    },
    {
      "name": "Республика Ингушетия",
      "count": 701,
      "iso": "RU-IN"
    },
    {
      "name": "Кабардино-Балкарская Республика",
      "count": 14058,
      "iso": "RU-KB"
    },
    {
      "name": "Карачаево-Черкесская Республика",
      "count": 7073,
      "iso": "RU-KC"
    },
    {
      "name": "Республика Северная Осетия - Алания",
      "count": 5705,
      "iso": "RU-SE"
    },
    {
      "name": "Чеченская Республика",
      "count": 5766,
      "iso": "RU-CE"
    },
    {
      "name": "Ставропольский край",
      "count": 45679,
      "iso": "RU-STA"
    }
  ]`;

function numberToColor(num, max) {
  let s = max / 140;
  let n = num / s;
  return hslToHex(140 - n, 70, 50);
}

function hslToHex(h, s, l) {
  l /= 100;
  const a = (s * Math.min(l, 1 - l)) / 100;
  const f = (n) => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color)
      .toString(16)
      .padStart(2, "0"); // convert to Hex and prefix "0" if needed
  };
  return `#${f(0)}${f(8)}${f(4)}`;
}

function init() {
  var map = new ymaps.Map("map", {
    center: [48.704272, 65.60203],
    zoom: 4,
    type: "yandex#map",
    controls: ["zoomControl"],
  });
  map.controls.get("zoomControl").options.set({ size: "small" });

  var objectManager = new ymaps.ObjectManager();

  // Загрузим регионы.
  ymaps.borders
    .load("RU", {
      lang: "ru",
      quality: 2,
    })
    .then(function (result) {
      // Очередь раскраски.
      var queue = [];
      // Создадим объект regions, где ключи это ISO код региона.
      var regions = result.features.reduce(function (acc, feature) {
        // Добавим ISO код региона в качестве feature.id для objectManager.
        var iso = feature.properties.iso3166;
        feature.id = iso;
        // Добавим опции региона по умолчанию.
        feature.options = {
          fillOpacity: 0.6,
          strokeColor: "#FFF",
          strokeOpacity: 0.5,
        };
        acc[iso] = feature;
        return acc;
      }, {});
      console.log(regions);

      let data = JSON.parse(SKFO);
      let max = 0;
      console.log(data);
      for (var i of data) {
        if (max < i.count) {
          max = i.count;
        }
      }
      element = document.getElementById("list");

      console.log(max);
      data = data.sort((a, b) => a.count - b.count);
      for (var i of data) {
        let color = numberToColor(i.count, max);
        regions[i.iso].options.fillColor = color;
        element.insertAdjacentHTML(
          "afterbegin",
          `<div class="item">
          <div class="count" style="background: ${color}">${i.count}</div>
          <div class="text">${i.name}</div>
        </div>`
        );
      }

      // Добавим регионы на карту.
      result.features = [];
      for (var reg in regions) {
        result.features.push(regions[reg]);
      }
      objectManager.add(result);
      map.geoObjects.add(objectManager);
    });
}
