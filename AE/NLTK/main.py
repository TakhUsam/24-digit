import nltk
from nltk import word_tokenize
from nltk.probability import FreqDist

import string

import re


f = open('pushkin-metel.txt', "r", encoding="utf-8")
text = f.read()

type(text)
len(text)


text = text[:300]

# перевод в единый регистр
text = text.lower()

string.punctuation


type(string.punctuation)
spec_chars = string.punctuation + '\n\xa0«»\t—…'
text = "".join([ch for ch in text if ch not in spec_chars])

text = re.sub('\n', '', text)


def remove_chars_from_text(text, chars):
    return "".join([ch for ch in text if ch not in chars])
text = remove_chars_from_text(text, spec_chars)
text = remove_chars_from_text(text, string.digits)


#Токенизация текста
text_tokens = word_tokenize(text)
print(type(text_tokens), len(text_tokens))
text_tokens[:10]

text = nltk.Text(text_tokens)
print(type(text))
text[:10]

#Расчёт растоты встречаемости слов
fdist = FreqDist(text)
fdist.most_common(5)
#[('и', 146), ('в', 101), ('не', 69), ('что', 54), ('с', 44)]

fdist.plot(30,cumulative=False)